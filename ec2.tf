resource "aws_instance" "App01" {
    ##ami = "ami-846144f8"
    ami = "${data.aws_ami.aws_linux.id}"
    instance_type = "t1.micro"
    subnet_id = "${aws_subnet.public_1a.id}"
    associate_public_ip_address = true
    vpc_security_group_ids = ["${aws_security_group.web_server.id}","${aws_security_group.allow_ssh.id}"]
    key_name = "pubkey"
    tags {
        Name = "App01"
    }
    


}

resource "aws_instance" "App02" {
    ##ami = "ami-846144f8"
    ami = "${data.aws_ami.aws_linux.id}"
    instance_type = "t1.micro"
    subnet_id = "${aws_subnet.public_1a.id}"
    associate_public_ip_address = true
    vpc_security_group_ids = ["${aws_security_group.web_server.id}","${aws_security_group.allow_ssh.id}"]
    key_name = "pubkey" 
    tags {
        Name = "App02"
    }
    user_data = "${data.template_file.bootstrap.rendered}"
}


resource "aws_instance" "DBandMQ" {
    ##ami = "ami-846144f8"
    ami = "${data.aws_ami.aws_linux.id}"
    instance_type = "t1.micro"
    subnet_id = "${aws_subnet.public_1a.id}"
    associate_public_ip_address = true
    vpc_security_group_ids = ["${aws_security_group.web_server.id}","${aws_security_group.allow_ssh.id}"]
    key_name = "pubkey"
    tags {
        Name = "DBandMQ"
    }
    user_data = "${data.template_file.bootstrap.rendered}"
}



resource "null_resource" remoteExecute01 {
    triggers{
        cluster_instance_ids = "${join(",", aws_instance.App01.*.id)}"
    }
    provisioner "remote-exec"{
        inline = [
            "cat /etc/hosts",
            "echo ${aws_instance.App01.private_ip}  App01 | sudo tee -a /etc/hosts",
            "echo ${aws_instance.App02.private_ip}  App02 | sudo tee -a /etc/hosts",
            "echo ${aws_instance.DBandMQ.private_ip}  DBandMQ | sudo tee -a /etc/hosts",
            "mkdir -p ~/app/enginev3/stateless-app/",
            "mkdir -p ~/app/enginev3/stateful-app/",
            "curl -O http://ec2-54-169-139-23.ap-southeast-1.compute.amazonaws.com/repository/com/pie/enginev3/stateful-app/1.0.0-SNAPSHOT/stateful-app-1.0.0-SNAPSHOT-dist.tar.gz",
            "curl -O http://ec2-54-169-139-23.ap-southeast-1.compute.amazonaws.com/repository/com/pie/enginev3/stateless-app/1.0.0-SNAPSHOT/stateless-app-1.0.0-SNAPSHOT-dist.tar.gz",
            "tar -xf /home/ec2-user/stateful-app-1.0.0-SNAPSHOT-dist.tar.gz  -C ~/app/enginev3/stateful-app"
        ]
        connection {
            user = "ec2-user"
            private_key = "${file("id_rsa")}"
            type = "ssh"
            host = "${aws_instance.App01.public_ip}"
        }
    }
}

resource "null_resource" remoteExecute02 {
    triggers{
        cluster_instance_ids = "${join(",", aws_instance.App01.*.id)}"
    }
    provisioner "remote-exec"{
        inline = [
            "cat /etc/hosts",
            "echo ${aws_instance.App01.private_ip}  App01 | sudo tee -a /etc/hosts",
            "echo ${aws_instance.App02.private_ip}  App02 | sudo tee -a /etc/hosts",
            "echo ${aws_instance.DBandMQ.private_ip}  DBandMQ | sudo tee -a /etc/hosts",
            "mkdir -p ~/app/enginev3/stateless-app/",
            "mkdir -p ~/app/enginev3/stateful-app/",
            "curl -O http://ec2-54-169-139-23.ap-southeast-1.compute.amazonaws.com/repository/com/pie/enginev3/stateful-app/1.0.0-SNAPSHOT/stateful-app-1.0.0-SNAPSHOT-dist.tar.gz",
            "curl -O http://ec2-54-169-139-23.ap-southeast-1.compute.amazonaws.com/repository/com/pie/enginev3/stateless-app/1.0.0-SNAPSHOT/stateless-app-1.0.0-SNAPSHOT-dist.tar.gz",
            "tar -xf /home/ec2-user/stateful-app-1.0.0-SNAPSHOT-dist.tar.gz  -C ~/app/enginev3/stateful-app"
        ]
        connection {
            user = "ec2-user"
            private_key = "${file("id_rsa")}"
            type = "ssh"
            host = "${aws_instance.App02.public_ip}"
        }
    }
}

resource "null_resource" remoteExecute03 {
    triggers{
        cluster_instance_ids = "${join(",", aws_instance.App01.*.id)}"
    }
    provisioner "remote-exec"{
        inline = [

        ]
        connection {
            user = "ec2-user"
            private_key = "${file("id_rsa")}"
            type = "ssh"
            host = "${aws_instance.DBandMQ.public_ip}"
        }
    }
}
resource "aws_key_pair" "key" {
  key_name   = "pubkey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHzATp/2KhhrVF0CiI6sHX7HA0z+JSAf+5JF5zdD7KnKsO9kS4Y6HV2vPPuV/z/IWIOLQeNOgXZQyC832oOdSAPu7/sag7PxpPXoXTqUJH+hc8zDUJ/WegX1dVhm3zZjU7TvvsjKJMUWO0c7TaRglebkcoMGzTMtU9WHF/7fJ8npOv4DSMC7Y7Ss1263vffpqnUpeBCsAHT6v+JuMsL6wEdYnQnY4GslmS3GTItQ1J2gNBlnMOyfVTOsyQNyw2sxE1AyvYvgxiZRZ1IYOth1al5uJQjEirjrb3llJgKQgMjwAX3zhPBa9E0UzyOx9YuaWJ2Yq8xP3OZ2Jh913KWlLT csenese@DESKTOP-1HRLKGS"
}



##Lookup AMI
data "aws_ami" "aws_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}


data "template_file" "bootstrap" {
  template = "${file("bootstrap.tpl")}"
    vars {
    }
}