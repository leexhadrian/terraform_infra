output "App01_ipaddress" {
  value = "${aws_instance.App01.public_ip}"
}

output "App02_ipaddress" {
  value = "${aws_instance.App02.public_ip}"
}

output "DBMQ_ipaddress" {
  value = "${aws_instance.DBandMQ.public_ip}"
}